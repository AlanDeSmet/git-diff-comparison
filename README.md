Git's diff algorithm speed
==========================

Alan De Smet, January 25, 2022



At of January 2021, Git supports 4 diff algorithms: patience, minimal, histogram, 

- myers - Based on Eugene Myers 1986 paper "[An O(ND) Difference Algorithm and
  Its
Variations](http://btn1x4.inf.uni-bayreuth.de/publications/dotor_buchmann/SCM/ChefRepo/DiffUndMerge/DAlgorithmVariations.pdf)".

- minimal - As myers, but willing to spend more effort to find a shorter diff.

- patience - Based on Bram Cohen's [patience
  diff](https://bramcohen.livejournal.com/73318.html) ([from around
2007](https://bramcohen.livejournal.com/37690.html)).

- histogram - A further refinement on patience.

The default is myers. But why? As far as I can tell, _generally_ speaking,
people expect patience and history to provide more human friendly output while
being functionally the same.  I haven't seen any reasoning given, but I do see
discussions of how the later diff algorithms are less efficient. Does it matter
in practice?

These scripts grab several versions of the Linux kernel source code, a large,
active project. For several pairings designed to offer different levels of
difference, it first tries to prime the disk cache by doing a myers comparison,
then runs each of the algorithms (including myers again) to check the time
spend and lines generated. This is comparing two different directories of Linux
source code, not two different tags/branches/revision in git.

The results, as run on my 2021 ThinkPad off an SSD, using git 2.25.1:


| algorithm | A     | B     | diff lines | wall  | user  | sys  | maxres  |
| :-------- | :---- | :---- | ---------: | ----: | ----: | ---: | ------: |
| myers     | v5.15 | v5.16 |    949,118 | 11.17 |  7.88 | 3.24 | 258,068 |
| minimal   | v5.15 | v5.16 |    933,980 | 11.24 |  7.89 | 3.30 | 258,240 |
| patience  | v5.15 | v5.16 |    935,440 | 11.74 |  8.37 | 3.31 | 257,956 |
| histogram | v5.15 | v5.16 |    936,298 | 11.36 |  7.99 | 3.33 | 258,220 |
|           |       |       |            |       |       |      |         |
| myers     | v5.0  | v5.16 | 15,128,798 | 27.62 | 23.21 | 4.23 | 257,012 |
| minimal   | v5.0  | v5.16 | 15,025,296 | 28.10 | 23.44 | 4.48 | 257,008 |
| patience  | v5.0  | v5.16 | 15,068,836 | 28.25 | 23.95 | 4.09 | 257,272 |
| histogram | v5.0  | v5.16 | 15,099,940 | 28.49 | 23.97 | 4.33 | 257,060 |
|           |       |       |            |       |       |      |         |
| myers     | v4.0  | v5.16 | 27,579,374 | 28.11 | 23.07 | 3.99 | 253,216 |
| minimal   | v4.0  | v5.16 | 27,460,250 | 27.47 | 23.36 | 3.91 | 253,024 |
| patience  | v4.0  | v5.16 | 27,516,062 | 27.04 | 23.22 | 3.63 | 253,168 |
| histogram | v4.0  | v5.16 | 27,554,358 | 27.37 | 23.67 | 3.56 | 253,372 |

- **algorithm** - diff algorithm in use
- **A** and **B** - the two versions of the Linux kernel compared
- **diff lines** - Lines of difference reported (lines starting with `+` or `-` but _not_ `+++ ` or `--- `)
- **wall** - Real world seconds of time for `git diff` to exit.
- **user** - CPU seconds of time spent in user space
- **sys** - CPU seconds of time spent in user kernel space
- **maxres** - maximum kilobytes of resident memory

My conclusion: don't worry about speed or memory in the slightest. Also, the
sizes of the resulting diff are close enough that I wouldn't worry about that
either. Change your default to whichever one generates diffs you like looking
at more reliably. Figuring that out is my next goal.
